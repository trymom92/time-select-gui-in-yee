use time_select::*;
use yew::prelude::*;

macro_rules! callback {
    ($state_var:ident.$method_name:ident) => {{
        let $state_var = $state_var.clone();
        Callback::from(move |_| $state_var.set((*$state_var).$method_name()))
    }};
}

fn main() {
    yew::start_app::<App>();
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <TimeUi/>
    }
}

#[function_component(TimeUi)]
fn time_ui() -> Html {
    let time = use_state(|| time!(04:20 am));
    let Time12hFormat {
        hours,
        minutes,
        is_pm,
    } = (*time).clone().into();
    html! {
        <div id="TimeUi">
            <div id="HoursUp" class="btn" onclick={ callback!(time.next_hour) }>{ "+" }</div>
            <div id="Hours">{ format!("{:02}", hours) }</div>
            <div id="HoursDown" class="btn" onclick={ callback!(time.previous_hour) }>{ "-" }</div>
            <div id="Colon">{ ":" }</div>
            <div id="MinutesUp" class="btn" onclick={ callback!(time.next_minute) }>{ "+" }</div>
            <div id="Minutes">{ format!("{:02}", minutes) }</div>
            <div id="MinutesDown" class="btn" onclick={ callback!(time.previous_minute) }>{ "-" }</div>
            <div id="AmPm" class="btn" onclick={ callback!(time.with_am_pm_flipped) }>{ if is_pm { "PM" } else { "AM" } }</div>
        </div>
    }
}
