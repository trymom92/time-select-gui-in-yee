# “Time Select” GUI in Yew

A simple GUI for selecting time, made to test [Yew](https://yew.rs) and compiling Rust to WASM.

![Firefox screenshot](screenshot.png "Firefox screenshot")

It uses [my “Time Select” kata](https://gitlab.com/mojicfg/time-select-kata) as back-end.
